import os
import json

import gitlab
import jira

from issue import JiraIssue

JIRA_SERVER = os.environ['JIRA_SERVER']
JIRA_USERNAME = os.environ['JIRA_USERNAME']
JIRA_PASSWORD = os.environ['JIRA_PASSWORD']

JIRA = jira.JIRA(
    options={
        'server': JIRA_SERVER,
        'mutual_authentication': 'DISABLED',
    },
    basic_auth=(
        JIRA_USERNAME,
        JIRA_PASSWORD
    )
)

import gitlab
GITLAB = gitlab.Gitlab(GITLAB_URL, GITLAB_TOKEN)

SNIPPET = GITLAB.snippets.get(3214)



def main():
    issues = JIRA.search_issues("labels = 'cki' AND status != Done AND status != Abandoned", maxResults=0)
    issues = [JiraIssue(JIRA, issue) for issue in issues]

    md_text = ''

    for has_labels in [False, True]:

        if has_labels:
            md_text += '## Done\n\n'
        else:
            md_text += '## Missing\n\n'

        md_text += '| Ticket | Creator | Labels | Title |\n'
        md_text += '| --- | --- | --- | --- |\n'

        # Sort them reversed so the newest are created last
        for issue in sorted(issues, key=lambda i: (i.creator_username, i.id)):
            labels = ', '.join([
                label[len('gitlab-'):]
                for label in issue.labels
                if label.startswith('gitlab-')
            ])

            if labels and not has_labels:
                continue
            if not labels and has_labels:
                continue

            md_text += (
                '| '
                f'[{issue.issue_id}](https://projects.engineering.redhat.com/browse/{issue.issue_id}) | '
                f'{issue.creator_username} | '
                f'{labels} | '
                f'{issue.title} | '
                '\n'
            )

    SNIPPET.files[0] = {'action': 'update', 'file_path': 'list.md', 'content': md_text}
    SNIPPET.save()

main()
