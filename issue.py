import re
from cached_property import cached_property
from cki_lib import misc


class JiraIssue:
    """Jira issue but nice."""

    MIGRATION_COMMENT = 'Ticket migrated to Gitlab - '

    field_mapping = {
        'epic': 'customfield_10006',
        'title': 'summary',
        'description': 'description',
        'status': 'status/name',
        'created': 'created',
        'creator_username': 'creator/name',
        'creator_email': 'creator/emailAddress',
        'creator_name': 'creator/displayName',
        'releases': 'fixVersions',
        'labels': 'labels',
        'id': 'id',
    }

    def __init__(self, client, issue_id):
        """Init. Issue_id as FASTMOVING-{}."""
        self.client = client
        self.issue_id = issue_id

    @cached_property
    def _issue(self):
        """Return Jira issue object."""
        # Allow passing a Issue object from a query instead of FASTMOVING-*
        if isinstance(self.issue_id, str):
            return self.client.issue(self.issue_id)
        else:
            return self.issue_id

    @cached_property
    def _fields(self):
        """Return issue fields."""
        return self._issue.raw['fields']

    def __getattr__(self, name):
        """Try to get the arguments by field_mapping."""
        attr_name = self.field_mapping.get(name)
        if attr_name:
            return misc.get_nested_key(self._fields, attr_name)

        return self.__dict__[name]

    def __repr__(self):
        """Nice repr."""
        return f'<{self.issue_id}: {self.title}>'

    @property
    def epic_obj(self):
        """Return Issue object for the epic."""
        return JiraIssue(self.client, self.epic)

    @property
    def comments(self):
        """Return all the issue comments."""
        field_mapping = {
            'author_username': 'author/name',
            'author_email': 'author/emailAddress',
            'author_name': 'author/displayName',
            'body': 'body',
            'created': 'created',
            'id': 'id',
        }
        return [
            {
                field: misc.get_nested_key(comment, value)
                for field, value in field_mapping.items()
            }
            for comment in self._fields.get('comment', {}).get('comments', [])
        ]

    @cached_property
    def data(self):
        """All the data in json."""
        return {
            'epic': self.epic_obj,
            'title': self.title,
            'description': self.description,
            'comments': self.comments,
            'status': self.status,
        }

    def notify_migrated(self, gitlab_issue):
        """Tag this issue as already migrated."""
        if 'gitlab-migrated' in self.labels:
            return

        self._issue.add_field_value(field='labels', value='gitlab-migrated')

        self.client.add_simple_link(
            self._issue,
            {
                'url': gitlab_issue.attributes['web_url'],
                'title': f'Migrated Gitlab issue {gitlab_issue.attributes["references"]["full"]}'
            }
        )

        self.client.add_comment(
            self._issue,
            self.MIGRATION_COMMENT + gitlab_issue.attributes['web_url']
        )


def parse_text(text):
    """Replace jira spacific syntax."""
    to_replace = [
        (re.compile(r'{quote}'), '>>>'),
        (re.compile(r'{code.*}'), '```'),
        (re.compile(r'\[\~([a-z]+)\]'), r'@\1'),
        (re.compile(r'\r'), ''),
        # users
        (re.compile('@imalerba'), '@inakimalerba'),
        (re.compile('@mhofmann'), '@mh21'),
        (re.compile('@dzickus'), '@dzickusrh'),
        (re.compile('@vkabatov'), '@veruu'),
        (re.compile('@okinst'), '@Toaster192'),
    ]

    for match, replace in to_replace:
        text = match.sub(replace, text)

    return text


class GitlabIssue:
    """Gitlab issue."""

    def __init__(self, issue):
        """Init."""
        self._issue = issue

    @property
    def attributes(self):
        """Expose Gitlab Issue attributes."""
        return self._issue.attributes

    @classmethod
    def match_issue_from_jira(cls, gitlab_project, jira_issue):
        """Try to find the jira issue on GitLab."""
        search_string = f'<!-- JiraIssueId:{jira_issue.issue_id} -->'
        issues = gitlab_project.issues.list(search=search_string)
        if len(issues) > 1:
            raise Exception('More than 1 issue found for ', jira_issue.issue_id)
        if issues:
            return issues[0]
        return None

    @staticmethod
    def match_comment_from_jira(gitlab_comments, jira_comment):
        """Try to find the jira_comment between the comments on the issue."""
        for comment in gitlab_comments:
            if f'<!-- JiraCommentId:{jira_comment["id"]}' in comment.body:
                return comment

        return None

    @classmethod
    def create_from_jira(cls, gitlab_project, jira_issue):
        """Create GitlabIssue from Jira Issue."""
        if jira_issue.status == 'Done':
            return

        description = parse_text(
            f'{jira_issue.description}'
            '\n\n---\n\n'
            f'JIRA ticket created by @{jira_issue.creator_username}\n'
            f'<!-- JiraIssueId:{jira_issue.issue_id} -->'
        )

        data = {
            #'confidential': True,
            'created_at': jira_issue.created.replace('+0000', 'Z'),  # Replace so it's not different than Gitlab's time
            'description': description,
            'labels': ['jira-migrated', f'status::{jira_issue.status}'],
            'title': jira_issue.title.replace('DW: ', ''),
        }

        for release in jira_issue.releases:
            data['labels'].append(f'release::{release["name"]}')

        # Try to find it in Gitlab project issues.
        issue = cls.match_issue_from_jira(gitlab_project, jira_issue)
        if issue:
            # Update it with the current data.
            for key, value in data.items():
                old_value = getattr(issue, key)
                if old_value != value:
                    setattr(issue, key, value)
                    issue.save()
        else:
            # Create it if doesn't exist
            issue = gitlab_project.issues.create(data)

        # Get issue comments to try to update them
        gl_issue_comments = issue.notes.list(all=True)

        # Add JIRA comments as notes
        for comment in jira_issue.comments:

            # Do not bridge the migration notice
            if JiraIssue.MIGRATION_COMMENT in comment['body']:
                continue

            body = parse_text(
                f'{comment["body"]}'
                f'\n\n---\n\n'
                f'JIRA Comment by: @{comment["author_username"]}\n'
                f'<!-- JiraCommentId:{comment["id"]} -->'
            )
            gl_comment = cls.match_comment_from_jira(gl_issue_comments, comment)
            if gl_comment:
                if gl_comment.body != body:
                    gl_comment.body = body
                    gl_comment.save()
            else:
                issue.notes.create({
                    'body': body,
                    'created_at': comment["created"],
                })

        # Close issue if it's done in JIRA
        # if jira_issue.status == 'Done':
        #     issue.state_event = 'close'
        #     issue.save()

        return cls(issue)
